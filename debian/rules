#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1


ADDONS := $(notdir $(wildcard src/addons/*))

%:
	dh $@ --with nodejs

override_dh_auto_build:
# requires node-pty bindings, unpackaged
	rm src/Terminal.integration.ts
	tsc --skipLibCheck --project .
	for addon in $(ADDONS); do tsc --skipLibCheck --project src/addons/$$addon; done
# Otherwise browserify-lite complains
	touch lib/addons/index.js
	mkdir -p dist
	cd dist && browserify-lite ../lib/xterm.js --outfile xterm.js --standalone Terminal
	cd dist && for addon in $(ADDONS); do mkdir -p addons/$$addon && browserify-lite ../lib/addons/$$addon/$$addon.js --outfile addons/$$addon/$$addon.js --standalone $$addon; done
# Undo browserify-lite workarounds
	sed -i -e 's/REQUIRE_NO_BROWSERIFY/require/g' dist/xterm.js dist/addons/*/*.js
# browserify-lite seems to generate a module map with indexes starting from zero, then tests
# the truth of the module ID when loading sub-parts - which then fails for whatever ends up
# as sub-module 0; not convinced I fully understand what is happening here, because it seems
# like this should break everything browserify-life produces; dropping this check makes it load;
	sed -i -e '/if [(]!id[)] throw new Error/d' dist/xterm.js dist/addons/*/*.js

	rm -f dist/addons/index.js
	cd src && find . -name '*.css' -exec cp -v --parents '{}' ../lib \;
	cd src && find . -name '*.css' -exec cp -v --parents '{}' ../dist \;

override_dh_auto_test:
# These tests depend on jsdom which can't (yet) be installed in Debian
# They pass when run locally though
	rm -f lib/Linkifier.test.js lib/utils/CharMeasure.test.js
	mocha lib/

# Upstream does not have tests for the browserified version, but you can test
# that manually by running this:
run-demo:
	test -e build || ln -sf dist build
	python -m SimpleHTTPServer & x=$$!; \
	chromium --temp-profile 'http://localhost:8000/demo/index.html'; kill $$!

override_dh_auto_clean:
	dh_auto_clean
	rm -rf node_modules lib dist build

override_dh_link:
# Deduplicate, link via dh_link
	rm -rf debian/node-xterm/usr/share/nodejs/xterm/lib/addons
	dh_link

get-orig-source:
	uscan --download-current-version --force-download --no-symlink
